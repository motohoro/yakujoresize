============
薬情リサイズ
============
薬情に載せる写真を、欲しい部分だけできるだけ拡大したうえで、画像の縦横比と背景色を調整するためのGIMPプラグイン。windowsでのみ動作確認してますが、一応OSチェックしてるので他でも大丈夫のはず

***************
インストール
***************
#. GIMP本体のインストール
    GIMP - Downloads http://www.gimp.org/downloads/

#.  `ここ <https://bitbucket.org/motohoro/yakujoresize/get/master.zip>`_ からダウンロード解凍。でてきたyakujoresize.pyをplug-insディレクトリに配置する

    GIMPにプラグインをインストールする方法 | GIMP2の使い方 http://gimp2-how-to-use.blogspot.jp/2012/03/gimp.html 

***************
使い方
***************

#. 画像を読み込みます

#. 背景処理など行った後、必要部分を矩形選択ツールで選択しておいて、メニューから画像-薬情リサイズを選択

#. 出てきたダイアログで縦横サイズ、背景色、保存ディレクトリ＆ファイル名を指定してＯＫボタンをクリック

#. 処理終了後、保存ディレクトリが開きます(windowsのみ)

windowsのみ
^^^^^^^^^^^^^^^^^^^^^^^
指定しておいたUSBメモリのルートを保存ディレクトリの初期値にすることが可能

 yakujoresize.pyのvolumelabel=u""をvolumelabel=u"HOGE"などと書き換える（UTF-8扱えるエディタを使うこと）

***************
困ったときは
***************
- GIMP全般やプラグインのインストール方法など一般的な事は `こちら <http://www.google.com/>`_ で。

- 本プラグインの動作不良やトラブル、機能の改善追加は `Issues <https://bitbucket.org/motohoro/yakujoresize/issues?status=new&status=open>`_ に登録して下さい
