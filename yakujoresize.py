#!/usr/bin/env python
# -*- coding: utf-8 -*-
# yakujoresize
# Copyright mituhiromatumoto 2013
# Licence GPL
# Version 1.0
# - initial release
#
# Installation : put the yakujoresize.py file in your $HOME/.gimp-2.n/plug-ins.
# On Linux and Mac OSX the file must be executable.
# Documentation : http://www.gimp.org/docs/python/index.html
import sys
import os
import urllib
import time
import re
from gimpfu import *
import math
import platform
volumelabel=u"USBメモリへのパス"
DEBUG=False
if DEBUG:
    fo = open(os.path.abspath(os.path.dirname(__file__))+r'\output.txt','w')
    sys.stdout=fo
    sys.stderr=fo
def initsavepath():
    if platform.system()=='Windows' and len(volumelabel)>0:
        import ctypes
        import string
        drives=[]
        kernel32 = ctypes.windll.kernel32
        bitmask = kernel32.GetLogicalDrives()
        for letter in string.uppercase:
            if bitmask & 1:
                drives.append(letter)
            bitmask >>= 1
        for drive in drives:
            '''
    About volumeNameBuffer
    http://www.velocityreviews.com/forums/t395046-re-file-system-iteration.html
            DRIVE_UNKNOWN=0
            DRIVE_NO_ROOT_DIR=1
            DRIVE_REMOVABLE=2
            DRIVE_FIXED=3
            DRIVE_REMOTE=4
            DRIVE_CDROM=5
            DRIVE_RAMDISK=6 
            '''
            volumeNameBuffer = ctypes.create_unicode_buffer(1024)
            fileSystemNameBuffer = ctypes.create_unicode_buffer(1024)
            serial_number = None
            max_component_length = None
            file_system_flags = None
            dpath = drive+":\\"
            dtype = kernel32.GetDriveTypeA(dpath)
            rc = kernel32.GetVolumeInformationW(
                ctypes.c_wchar_p(dpath),
                volumeNameBuffer,
                ctypes.sizeof(volumeNameBuffer),
                serial_number,
                max_component_length,
                file_system_flags,
                fileSystemNameBuffer,
                ctypes.sizeof(fileSystemNameBuffer)
            )
            if dtype==2 and volumeNameBuffer.value.startswith(volumelabel):
                return dpath
    return "."
def yakujoresize_main(timg, tdrawable,y_width=280,y_height=210,bg_color=(0,0,0),dir=".",savefilename=""):
    uri = pdb.gimp_image_get_imported_uri(timg)
    try:
        filename = os.path.basename(uri)
    except TypeError:
        filename="yakujoresize_image"
    basenameuri,ext = os.path.splitext(filename)
    basename = urllib.unquote(basenameuri)
    timg.undo_group_start()
    num_bytes,colormap=pdb.gimp_image_get_colormap(timg)
    if num_bytes>0:
        pdb.gimp_image_convert_rgb(timg)
    non_empty, x1, y1, x2, y2 = pdb.gimp_selection_bounds(timg)
    if non_empty==False:
        pdb.gimp_message(u"背景処理して切り出す範囲を選択して下さい")
        timg.undo_group_end()
        return
    if re.search(r"\s",dir) or re.search(r"\s",savefilename):
        pdb.gimp_message(u"保存ディレクトリのパスとファイル名に空白は使わないで下さい")
        timg.undo_group_end()
        return
    select_width=x2-x1
    select_height=y2-y1
    y2_width=y_width-2
    y2_height=y_height-2
    height_scale = 1.0*y2_height/select_height
    width_scale = 1.0*y2_width/select_width
    if height_scale>width_scale:
        scaling=width_scale
    else:
        scaling=height_scale
    newWidth=select_width * scaling
    newHeight =select_height * scaling
    pdb.gimp_image_crop(timg, select_width, select_height, x1, y1)
    pdb.gimp_image_scale(timg, math.floor(newWidth), math.floor(newHeight))
    pdb.gimp_image_resize(timg, y_width, y_height, (y_width-newWidth)/2, (y_height-newHeight)/2)
    pdb.gimp_context_set_background(bg_color)
    pdb.gimp_layer_resize_to_image_size(tdrawable)
    pdb.gimp_image_flatten(timg)
    tdrawable2 = pdb.gimp_image_get_active_drawable(timg)
    if not savefilename:
        savefilename = basename+str(int(time.time()))
    savefilepath = os.path.join(dir,savefilename+".jpg")
    print savefilepath
    exifnum,exifdatas = pdb.gimp_image_get_parasite_list(timg)
    for exifdata in exifdatas:
        pdb.gimp_image_detach_parasite(timg,exifdata)
    pdb.file_jpeg_save(timg, tdrawable2,  savefilepath, savefilepath, 
                               0.9, 0, 0, 0, "", 0, 0, 0, 0)    
    timg.undo_group_end()
    if platform.system()=='Windows':
        os.system(r"start "+dir.encode('cp932'))
    return
register(
        "yakujo_resize",
        "薬情写真サイズに変更して保存",
        "薬情写真サイズに変更して保存plugin",
        "MituhiroMatumoto",
        "MituhiroMatumoto",
        "2013",
        u"<Image>/Image/薬情リサイズ",
        "RGB*, GRAY*, INDEXED*",
        [
          (PF_INT, "yakujo_width", u"画像幅", 280),
          (PF_INT, "yakujo_height", u"画像高さ", 210),
          (PF_COLOR,  "bg_color", u"背景色", (255, 255, 255)),
          (PF_DIRNAME,"dir",u"保存ディレクトリ",initsavepath()),
          (PF_STRING, "filename", u"ファイル名(空白:今のファイル名)", ""),
        ],
        [],
        yakujoresize_main)
main()
